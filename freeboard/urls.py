from django.urls import path

from freeboard import views

app_name = 'freeboard'
urlpatterns = [

    path('', views.FreeBoardView.as_view(), name='freeboard_view'),
    path('insertfreeboard', views.InsertBoard_Random, name='insert_free_board'),
    path('jstest', views.JS_Test, name='jstest'),
]