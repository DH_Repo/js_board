from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView

from freeboard.models import Free_Board



class FreeBoardView(TemplateView):
    template_name = 'freeboard/freeboardlist.html'
    def get_context_data(self, **kwargs):
        context = super(FreeBoardView, self).get_context_data(**kwargs)
        context['freeboard_list'] = Free_Board.objects.all()
        return context

def InsertBoard_Random(request):
    Free_Board.objects.create(subject="첫번째 글", content="반갑습니다.",
                              user_id=1)
    return render(request, 'home.html')

def JS_Test(request):
    myList = []

    return render(request, 'freeboard/js_test_view.html',{'mylist':myList})