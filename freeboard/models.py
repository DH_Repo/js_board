from django.db import models

# Create your models here.
class Free_Board(models.Model):
    subject = models.CharField(max_length=50, null=False, blank=False)
    content = models.CharField(max_length=500, null=False, blank=False)
    createdDate = models.DateTimeField(max_length=50, null=False, blank=False, auto_now=True)
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE, null=False, default='')
    class Meta:
        db_table = 'free_board'
