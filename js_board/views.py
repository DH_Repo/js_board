from django.shortcuts import render
from django.views.generic import TemplateView


# 클래스 베이스 뷰
class HomeView(TemplateView):
    template_name = 'home.html'

# 함수 뷰
def Home(request):
    myList = ['동현', '지수']
    return render(request, 'home.html',{'mylist':myList})